import logging

from httpx import Request, AsyncBaseTransport, Response
from kiota_http.middleware import BaseMiddleware


class RequestLogger(BaseMiddleware):

    def __init__(self):
        super().__init__()
        self._log = logging.getLogger(self.__class__.__name__)

    async def send(self, request: Request, transport: AsyncBaseTransport) -> Response:
        self._log.debug(f'{request.method} {request.url}')
        if request.method in ['PUT', 'PATCH', 'POST']:
            self._log.debug(request.content.decode())

        response = await super().send(request, transport)
        return response
