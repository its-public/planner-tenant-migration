import argparse
import asyncio
import logging
from typing import Tuple

from dotenv import dotenv_values

from PlannerMigration import PlannerMigration
from graph import Graph
from object_mapper import CsvObjectIDMapper

LOGGER: logging.Logger


def _read_env(env_file: str = None) -> Tuple[bool, dict]:
    # Default env_file is None, which effectively means ".env"
    env_config = dotenv_values(env_file, verbose=True)
    LOGGER.info(f'Environment loaded from "{".env" if env_file is None else env_file}"')

    return True, env_config


async def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--src-config', help='Source tenant config file')
    parser.add_argument('-d', '--dst-config', help='Destination tenant config file')
    parser.add_argument('-g', '--group-id', help='UID of the group to look for Planner plans')
    parser.add_argument('--obj-map-file', help='Optional CSV file with mappings from old to new Object IDs')

    args = parser.parse_args()

    _, src_tenant_config = _read_env(args.src_config)
    _, dst_tenant_config = _read_env(args.dst_config)
    src_graph = Graph(src_tenant_config)
    dst_graph = Graph(dst_tenant_config)

    # Comment out this check if implementation is not using CsvObjectIDMapper
    if args.obj_map_file is None:
        raise Exception('Path to CSV with Object ID mapping is not specified')
    obj_mapper = CsvObjectIDMapper(args.obj_map_file)

    migration = PlannerMigration(src_graph, dst_graph, obj_mapper)

    # Top Team
    src_group_id = args.group_id
    dst_group_id = obj_mapper.map_object_id(src_group_id)

    if dst_group_id is None:
        raise Exception('Destination group ID not found')

    for pl in await src_graph.list_plans_by_group(src_group_id):
        # await migration.read_plan(pl.id)
        await migration.copy_plan(pl.id, dst_group_id)

if __name__ == '__main__':
    LOGGER = logging.getLogger()
    # Turn off spamming about session tokens
    azure_id_logger = logging.getLogger('azure.identity._internal.interactive')
    azure_id_logger.setLevel(level=logging.WARNING)

    middleware_logger = logging.getLogger('RequestLogger')
    middleware_logger.setLevel(level=logging.DEBUG)

    logging.basicConfig(level=logging.INFO)

    asyncio.run(main())
