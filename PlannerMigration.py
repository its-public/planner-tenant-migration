import functools
import logging
from time import sleep
from typing import Tuple

from async_lru import alru_cache
from msgraph.generated.groups.item.threads.item.reply.reply_post_request_body import ReplyPostRequestBody
from msgraph.generated.models.conversation_thread import ConversationThread
from msgraph.generated.models.identity import Identity
from msgraph.generated.models.identity_set import IdentitySet
from msgraph.generated.models.item_body import ItemBody
from msgraph.generated.models.planner_applied_categories import PlannerAppliedCategories
from msgraph.generated.models.planner_assignments import PlannerAssignments
from msgraph.generated.models.planner_bucket import PlannerBucket
from msgraph.generated.models.planner_category_descriptions import PlannerCategoryDescriptions
from msgraph.generated.models.planner_checklist_items import PlannerChecklistItems
from msgraph.generated.models.planner_plan_details import PlannerPlanDetails
from msgraph.generated.models.planner_task import PlannerTask
from msgraph.generated.models.planner_task_details import PlannerTaskDetails
from msgraph.generated.models.post import Post

from graph import Graph
from object_mapper import AbstractObjectIDMapper, SameTenantObjectIDMapper


class PlannerMigration:

    def __init__(self,
                 src_graph: Graph,
                 dst_graph: Graph,
                 object_id_mapper: AbstractObjectIDMapper = None,
                 dry_run: bool = False):
        self._log = logging.getLogger(self.__class__.__name__)
        self._src_graph = src_graph
        self._dst_graph = dst_graph
        self._mapper = SameTenantObjectIDMapper() if object_id_mapper is None else object_id_mapper
        self._dry_run = dry_run

    def _copy_assignments(self, src_assignments: PlannerAssignments) -> PlannerAssignments:
        assignments_data = {}

        # Note that order of assignments is ignored
        for object_id, assignment in src_assignments.additional_data.items():
            new_object_id = self._mapper.map_object_id(object_id)
            # Either given user is not valid anymore or their object ID is
            # missing from Object ID mapper
            if new_object_id is None:
                self._log.warning(f'User assignment map not found: {object_id}')
                continue

            assignments_data[new_object_id] = {
                '@odata.type': '#microsoft.graph.plannerAssignment',
                # 'assignedDateTime': assignment.get('assignedDateTime'),
                'orderHint': ' !'
            }

        dst_assignments = PlannerAssignments(
            additional_data=assignments_data
        )
        return dst_assignments

    def _copy_identity_set(self, id_set: IdentitySet) -> IdentitySet | None:
        if id_set is None:
            return None

        return IdentitySet(
            user=Identity(
                display_name=id_set.user.display_name,
                id=self._mapper.map_object_id(id_set.user.id)
            )
        )

    @staticmethod
    def _copy_categories(src_categories: PlannerAppliedCategories) -> PlannerAppliedCategories:
        return PlannerAppliedCategories(
            additional_data=dict(src_categories.additional_data)
        )

    @staticmethod
    def _copy_checklist(src_checklist: PlannerChecklistItems) -> PlannerChecklistItems:
        dst_list = {}
        sorted_items = sorted(src_checklist.additional_data.values(), key=lambda i: i['orderHint'], reverse=False)
        ord_no = 1
        for item in sorted_items:
            dst_list[str(ord_no)] = {
                '@odata.type': '#microsoft.graph.plannerChecklistItem',
                'isChecked': item['isChecked'],
                'orderHint': ' !',
                'title': item['title']
            }
            ord_no += 1

        return PlannerChecklistItems(
            additional_data=dst_list
        )

    async def _copy_bucket_tasks(self, src_bucket_id: str, dst_plan_id: str, dst_bucket_id: str):
        src_tasks = await self._src_graph.list_tasks_by_bucket(src_bucket_id)
        src_tasks = sorted(src_tasks, key=lambda t: t.id, reverse=True)
        for task in src_tasks:
            await self.copy_task(task.id, dst_plan_id, dst_bucket_id)
            sleep(1)

    async def _copy_buckets(self, src_plan_id: str, dst_plan_id: str) -> Tuple[list[PlannerBucket], dict[str, str]]:
        """
        Copies buckets from source plan to destination
        :param src_plan_id: Source plan ID
        :param dst_plan_id: Destination plan ID
        :return: Tuple, where:
                    - first element is a list of new buckets
                    - second element is a map from source bucket ID to new bucket ID
        """
        old_buckets = await self._src_graph.list_buckets(src_plan_id)
        # Sort buckets by their order_hint and then add them one by one
        # with 'put at the top' logic
        # Ref. to https://learn.microsoft.com/en-us/graph/api/resources/planner-order-hint-format?view=graph-rest-1.0
        old_buckets = sorted(old_buckets, key=lambda i: i.order_hint, reverse=True)
        new_buckets = []
        bucket_map = {}
        for b in old_buckets:
            self._log.info(f'Create bucket "{b.name}"')
            request_body = PlannerBucket(
                name=b.name,
                plan_id=dst_plan_id,
                order_hint=' !'
            )

            result = await self._dst_graph.create_bucket(request_body)
            new_buckets.append(result)
            bucket_map[b.id] = result.id

        return new_buckets, bucket_map

    async def _copy_plan_details(self, src_plan_id: str, dst_plan_id: str):
        src_plan_details = await self._src_graph.get_plan_details(src_plan_id)
        dst_plan_details = await self._dst_graph.get_plan_details(dst_plan_id)
        src_category_descriptions = src_plan_details.category_descriptions
        dst_category_descriptions = PlannerCategoryDescriptions()

        for i in range(1, 25):
            val = getattr(src_category_descriptions, f'category{i}')
            setattr(dst_category_descriptions, f'category{i}', val)

        new_details = PlannerPlanDetails(
            additional_data={
                '@odata.etag': dst_plan_details.additional_data.get('@odata.etag')
            },
            category_descriptions=dst_category_descriptions
        )

        await self._dst_graph.update_plan_detail(dst_plan_id, new_details)

    async def _copy_task_conversation(self, src_task: PlannerTask, dst_group_id: str) -> ConversationThread | None:
        """
        Creates a copy of conversation thread in destination group
        :param src_task: Task with conversation which should be cloned
        :param dst_group_id: Group where conversation clone should be created
        :return: Cloned conversation thread instance or None, if source task has no conversation
        """

        def insert_on_behalf_to_body(body: str, name: str) -> str:
            prefix = ''
            if body.startswith('<html>'):
                body = body.removeprefix('<html>')
                prefix = '<html>'
            if body.startswith('<body>'):
                body = body.removeprefix('<body>')
                prefix += '<body>'

            while body.startswith('<div>'):
                body = body.removeprefix('<div>')
                prefix += '<div>'

            return f'{prefix}<div>On behalf of <strong>{name}</strong>:</div>{body}'

        if src_task.conversation_thread_id is None:
            return None

        src_group_id = await self._get_group_id_by_plan_id(self._src_graph, src_task.plan_id)
        conversation = await self._src_graph.get_conversation_thread(src_group_id, src_task.conversation_thread_id)
        posts = await self._src_graph.list_conversation_posts(src_group_id, conversation.id)

        if len(posts) == 0:
            return None

        # Sort posts in chronological order
        posts = sorted(posts, key=lambda k: k.received_date_time, reverse=False)

        first_post = posts[0]
        new_conversation = ConversationThread(
            topic=conversation.topic,
            posts=[
                Post(
                    body=ItemBody(
                        content_type=first_post.body.content_type,
                        content=insert_on_behalf_to_body(first_post.body.content,
                                                         first_post.from_.email_address.name)
                    )
                )]
        )

        new_conversation = await self._dst_graph.create_conversation_thread(dst_group_id, new_conversation)
        sleep(2) # Wait for some time because immediate replies may return 404 Not found for this thread

        for i in range(1, len(posts)):
            post = posts[i]
            reply = ReplyPostRequestBody(
                post=Post(
                    body=ItemBody(
                        content_type=post.body.content_type,
                        content=insert_on_behalf_to_body(post.body.content, post.from_.email_address.name)
                    )
                )
            )
            await self._dst_graph.create_conversation_thread_reply(
                dst_group_id,
                new_conversation.id,
                reply)

        return new_conversation

    async def _copy_task_details(self, src_task_id: str, dst_task_id: str):
        src_task_details = await self._src_graph.get_task_details(src_task_id)
        dst_task_details = await self._dst_graph.get_task_details(dst_task_id)

        new_details = PlannerTaskDetails(
            preview_type=src_task_details.preview_type,
            description=src_task_details.description,
            checklist=self._copy_checklist(src_task_details.checklist),
        )
        new_details.additional_data['@odata.etag'] = dst_task_details.additional_data.get('@odata.etag')
        await self._dst_graph.update_task_details(dst_task_id, new_details)

    @staticmethod
    @alru_cache()
    async def _get_group_id_by_plan_id(graph: Graph, plan_id: str) -> str:
        """
        Returns Group ID by Plan ID.
        :param plan_id:
        :return:
        """
        plan = await graph.get_plan(plan_id)
        return plan.container.container_id

    async def copy_task(self,
                        src_task_id: str,
                        dst_plan_id: str,
                        dst_bucket_id: str = None,
                        order_hint: str = ' !') -> PlannerTask:
        """
        Copy single task with its details
        :param src_task_id:
        :param dst_plan_id:
        :param dst_bucket_id:
        :param order_hint:
        :return:
        """
        src_task = await self._src_graph.get_task(src_task_id)

        conversation_id = None
        if src_task.conversation_thread_id is not None:
            dst_group_id = await self._get_group_id_by_plan_id(self._dst_graph, dst_plan_id)
            task_conversation = await self._copy_task_conversation(src_task, dst_group_id)
            conversation_id = task_conversation.id

        new_task = PlannerTask(
            applied_categories=self._copy_categories(src_task.applied_categories),
            assignments=self._copy_assignments(src_task.assignments),
            bucket_id=dst_bucket_id,
            bucket_task_board_format=src_task.bucket_task_board_format,
            conversation_thread_id=conversation_id,
            due_date_time=src_task.due_date_time,
            order_hint=order_hint,
            percent_complete=src_task.percent_complete,
            plan_id=dst_plan_id,
            priority=src_task.priority,
            progress_task_board_format=src_task.progress_task_board_format,
            start_date_time=src_task.start_date_time,
            title=src_task.title
        )
        dst_task = await self._dst_graph.create_task(new_task)
        await self._copy_task_details(src_task_id, dst_task.id)

        return dst_task

    async def copy_plan(self, src_plan_id: str, dst_group_id: str):
        src_plan = await self._src_graph.get_plan(src_plan_id)
        dst_plan = await self._dst_graph.create_plan(dst_group_id, src_plan.title)

        await self._copy_plan_details(src_plan_id, dst_plan.id)

        # Copy plan buckets
        new_buckets, bucket_map = await self._copy_buckets(src_plan.id, dst_plan.id)

        # Copy plan tasks, bucket by bucket
        for src_bucket_id, dst_bucket_id in bucket_map.items():
            await self._copy_bucket_tasks(src_bucket_id, dst_plan.id, dst_bucket_id)

    async def read_plan(self, plan_id: str) -> None:
        plan = await self._src_graph.get_plan(plan_id)
        await self._src_graph.get_plan_details(plan_id)
        buckets = await self._src_graph.list_buckets(plan_id)
        for b in buckets:
            try:
                tasks = await self._src_graph.list_tasks_by_bucket(b.id)
            except Exception as ex:
                self._log.exception(f'Error listing tasks for bucket {b.id}: {ex}')
                continue

            for t in tasks:
                try:
                    await self._src_graph.get_task_details(t.id)
                except Exception as ex:
                    self._log.exception(f'Error reading task {t.id} details: {ex}')

                if t.conversation_thread_id is not None:
                    try:
                        await self._src_graph.get_conversation_thread(plan.container.container_id,
                                                                      t.conversation_thread_id)
                    except Exception as ex:
                        self._log.exception(f'Error reading conversation thread for task {t.id}: {ex}')
