import csv
from typing import overload
from typing import TextIO


class AbstractObjectIDMapper:

    def map_object_id(self, src_object_id: str) -> str: # noqa
        raise NotImplemented()


class SameTenantObjectIDMapper(AbstractObjectIDMapper):
    """
    Object ID mapper maps ObjectIDs from source tenant to
    destination tenant. Simplistic ObjectIDMapper returns
    same Object ID. I.e. it can be used to copy planner
    entities within the same tenant.
    """

    def map_object_id(self, src_object_id: str) -> str: # noqa
        return src_object_id


class CsvObjectIDMapper(AbstractObjectIDMapper):
    """
    Loads Object IDs from external CSV file.
    File is supposed to contain two rows: old and new object ID
    accordingly.
    """

    def __init__(self, map_file_name: str):
        self._map = {}
        with open(map_file_name, 'r') as f:
            reader = csv.reader(f, delimiter=',', quotechar='"')
            for row in reader:
                self._map[row[0]] = row[1]

    def map_object_id(self, src_object_id: str) -> str:
        return self._map.get(src_object_id)
