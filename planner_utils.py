import http
import logging

from msgraph.generated.models.o_data_errors.o_data_error import ODataError
from msgraph.generated.models.planner_plan import PlannerPlan

from graph import Graph

LOGGER = logging.getLogger('planner_utils')


async def list_plans_for_groups(graph: Graph) -> list[PlannerPlan]:
    """
    Async generator which iterates over all groups in tenant and lists
    plans in them.
    :param graph: Graph instance with initialized values for given tenant
    :return:
    """
    async for gr in graph.list_groups():
        try:
            plans = await graph.list_plans_by_group(gr.id)
            for pl in plans:
                yield pl
        except ODataError as ex:
            if ex.response_status_code == http.HTTPStatus.FORBIDDEN:
                LOGGER.warning(f'Access to plans in group "{gr.display_name}" is forbidden.')
            else:
                LOGGER.warning(f'Unable to list plans in group "{gr.display_name}": {ex.message}')
