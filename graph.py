import logging
from datetime import datetime, timezone

from azure.identity import InteractiveBrowserCredential, TokenCachePersistenceOptions
from kiota_abstractions.base_request_configuration import BaseRequestConfiguration
from kiota_authentication_azure.azure_identity_authentication_provider import AzureIdentityAuthenticationProvider
from msgraph import GraphServiceClient, GraphRequestAdapter
from msgraph.generated.groups.item.threads.item.reply.reply_post_request_body import ReplyPostRequestBody
from msgraph.generated.models.conversation_thread import ConversationThread
from msgraph.generated.models.group_collection_response import GroupCollectionResponse
from msgraph.generated.models.identity_governance.task_collection_response import TaskCollectionResponse
from msgraph.generated.models.planner import Planner
from msgraph.generated.models.planner_assignments import PlannerAssignments
from msgraph.generated.models.planner_bucket import PlannerBucket
from msgraph.generated.models.planner_plan import PlannerPlan
from msgraph.generated.models.planner_plan_container import PlannerPlanContainer
from msgraph.generated.models.planner_plan_details import PlannerPlanDetails
from msgraph.generated.models.planner_task import PlannerTask
from msgraph.generated.models.planner_task_details import PlannerTaskDetails
from msgraph.generated.models.post import Post
from msgraph.generated.planner.buckets.item.planner_bucket_item_request_builder import PlannerBucketItemRequestBuilder
from msgraph.generated.planner.plans.item.details.details_request_builder import \
    DetailsRequestBuilder as PlanDetailsRequestBuilder
from msgraph.generated.planner.plans.item.planner_plan_item_request_builder import PlannerPlanItemRequestBuilder
from msgraph.generated.planner.tasks.item.details.details_request_builder import \
    DetailsRequestBuilder as TaskDetailsRequestBuilder
from msgraph_core import GraphClientFactory

from middleware import RequestLogger


class Graph:
    settings: dict
    device_code_credential: InteractiveBrowserCredential
    client: GraphServiceClient

    def __init__(self, config: dict):
        self.settings = config
        self._log = logging.getLogger(self.__class__.__name__)
        client_id = self.settings['clientId']
        tenant_id = self.settings['tenantId']
        graph_scopes = self.settings['graphUserScopes'].split(',')

        cache_options = TokenCachePersistenceOptions()
        self.device_code_credential = InteractiveBrowserCredential(tenant_id=tenant_id,
                                                                   client_id=client_id,
                                                                   cache_options=cache_options)
        auth_provider = AzureIdentityAuthenticationProvider(self.device_code_credential,
                                                            scopes=graph_scopes)
        middleware = GraphClientFactory.get_default_middleware(options=None)
        middleware.append(RequestLogger())
        http_client = GraphClientFactory.create_with_custom_middleware(middleware)
        adapter = GraphRequestAdapter(auth_provider, http_client)

        self.client = GraphServiceClient(request_adapter=adapter)

    @staticmethod
    def _clean_request_config_headers(config: BaseRequestConfiguration):
        """
        Ref. to a bug on GitHub
        https://github.com/microsoftgraph/msgraph-sdk-python/issues/532
        :return:
        """
        config.headers.remove('If-Match')
        config.headers.remove('Prefer')

    async def create_bucket(self, new_bucket: PlannerBucket):
        result = await self.client.planner.buckets.post(new_bucket)
        return result

    async def create_conversation_thread(self, group_id: str, thread: ConversationThread) -> ConversationThread:
        return await self.client.groups.by_group_id(group_id).threads.post(thread)

    async def create_conversation_thread_reply(self, group_id: str, thread_id: str, post: ReplyPostRequestBody) -> None:
        await self.client.groups.by_group_id(group_id).threads.by_conversation_thread_id(thread_id).reply.post(post)

    async def create_plan(self, group_id: str, title: str) -> PlannerPlan | None:
        request_body = PlannerPlan(
            container=PlannerPlanContainer(
                url=f'https://graph.microsoft.com/v1.0/groups/{group_id}'
            ),
            title=title
        )
        result = await self.client.planner.plans.post(request_body)
        return result

    async def create_task(self, task: PlannerTask) -> PlannerTask | None:
        self._log.debug(f'Create new task. Plan ID: {task.plan_id} Bucket ID: {task.bucket_id}')
        result = await self.client.planner.tasks.post(task)
        return result

    async def delete_bucket(self, bucket: PlannerBucket) -> None:
        request_configuration = (PlannerBucketItemRequestBuilder
                                 .PlannerBucketItemRequestBuilderDeleteRequestConfiguration())
        request_configuration.headers.add('If-Match', bucket.additional_data.get('@odata.etag'))
        await self.client.planner.buckets.by_planner_bucket_id(bucket.id).delete(
            request_configuration=request_configuration)

    async def delete_bucket_by_id(self, bucket_id) -> None:
        bucket = await self.get_bucket(bucket_id)
        if bucket is None:
            return
        await self.delete_bucket_by_id(bucket.id)

    async def delete_plan(self, plan_id: str) -> None:
        plan = await self.get_plan(plan_id)
        if plan is None:
            return

        request_configuration = PlannerPlanItemRequestBuilder.PlannerPlanItemRequestBuilderDeleteRequestConfiguration()
        request_configuration.headers.add('If-Match', plan.additional_data.get('@odata.etag'))

        await self.client.planner.plans.by_planner_plan_id(plan_id).delete(
            request_configuration=request_configuration)

    async def get_conversation_thread(self, group_id: str, thread_id: str) -> ConversationThread | None:
        response = await self.client.groups.by_group_id(group_id).threads.by_conversation_thread_id(thread_id).get()
        return response

    async def get_bucket(self, bucket_id: str) -> PlannerBucket | None:
        self._log.info(f'Get Bucket ID {bucket_id}')
        return await self.client.planner.buckets.by_planner_bucket_id(bucket_id).get()

    async def get_plan(self, plan_id: str) -> PlannerPlan | None:
        self._log.info(f'Get Plan ID {plan_id}')
        return await self.client.planner.plans.by_planner_plan_id(plan_id).get()

    async def get_plan_details(self, plan_id: str) -> PlannerPlanDetails:
        self._log.info(f'Get Plan ID {plan_id} details')
        return await self.client.planner.plans.by_planner_plan_id(plan_id).details.get()

    async def get_task(self, task_id: str) -> PlannerTask | None:
        self._log.info(f'Get task ID {task_id}')
        return await self.client.planner.tasks.by_planner_task_id(task_id).get()

    async def get_task_details(self, task_id: str) -> PlannerTaskDetails:
        self._log.info(f'Get task ID {task_id} details')
        planner_response = await self.client.planner.tasks.by_planner_task_id(task_id).details.get()
        return planner_response

    async def list_conversation_posts(self, group_id: str, thread_id: str) -> list[Post] | None:
        response = await (self.client.groups.by_group_id(group_id)
                          .threads.by_conversation_thread_id(thread_id).posts.get())
        if response.odata_next_link is not None:
            raise NotImplemented('Paging through conversation posts is not implemented')
        return response.value

    async def list_buckets(self, plan_id: str) -> list[PlannerBucket]:
        self._log.info('List buckets')
        planner_response = await self.client.planner.plans.by_planner_plan_id(plan_id).buckets.get()
        if planner_response.odata_next_link is not None:
            raise NotImplemented('Paging through buckets is not implemented yet')
        return planner_response.value

    async def list_groups(self):
        """
        Async generator which returns all groups in a Tenant
        :return: iterator over groups
        """
        group_response = await self.client.groups.get()
        for gr in group_response.value:
            yield gr

        while group_response.odata_next_link is not None:
            request_info = self.client.groups.to_get_request_information()
            request_info.url = group_response.odata_next_link
            group_response = await self.client.request_adapter.send_async(request_info, GroupCollectionResponse, {}) # noqa
            for gr in group_response.value:
                yield gr

    async def list_plans_by_group(self, group_id: str) -> list[Planner] | None:
        group_response = await self.client.groups.by_group_id(group_id).planner.plans.get()
        return group_response.value

    async def list_tasks(self, plan_id: str) -> list[PlannerTask]:
        planner_response = await self.client.planner.plans.by_planner_plan_id(plan_id).tasks.get()
        if planner_response.odata_next_link is not None:
            raise NotImplemented('Paging through tasks is not implemented yet')
        return planner_response.value

    async def list_tasks_by_user_id(self, user_id: str):
        """
        Available in beta version of Graph API only
        :param user_id:
        :return:
        """
        planner_response = await self.client.users.by_user_id(user_id).planner.tasks.get()
        return planner_response.value

    async def list_tasks_by_bucket(self, bucket_id: str) -> list[PlannerTask]:
        self._log.info(f'Read bucket ID {bucket_id} tasks')
        result_tasks = []
        planner_response = await self.client.planner.buckets.by_planner_bucket_id(bucket_id).tasks.get()
        result_tasks.extend(planner_response.value)
        while planner_response.odata_next_link is not None:
            request_info = (self.client.planner.buckets.by_planner_bucket_id(bucket_id)
                            .tasks.to_get_request_information())
            request_info.url = planner_response.odata_next_link
            planner_response = await self.client.request_adapter.send_async(request_info, TaskCollectionResponse, {}) # noqa
            result_tasks.extend(planner_response.value)

        return result_tasks

    async def update_plan_detail(self, plan_id: str, details: PlannerPlanDetails):
        request_configuration = PlanDetailsRequestBuilder.DetailsRequestBuilderPatchRequestConfiguration()
        self._clean_request_config_headers(request_configuration)
        request_configuration.headers.add('If-Match', details.additional_data.get('@odata.etag'))
        result = await (self.client.planner.plans.by_planner_plan_id(plan_id)
                        .details.patch(details, request_configuration))

        return result

    async def update_task_details(self, task_id: str, details: PlannerTaskDetails) -> PlannerTaskDetails:
        request_configuration = TaskDetailsRequestBuilder.DetailsRequestBuilderPatchRequestConfiguration()
        self._clean_request_config_headers(request_configuration)
        request_configuration.headers.add('Prefer', 'return=representation')
        request_configuration.headers.add('If-Match', details.additional_data.get('@odata.etag'))

        result = await (self.client.planner.tasks.by_planner_task_id(task_id)
                        .details.patch(details,
                        request_configuration=request_configuration))

        return result

    async def bug_test(self, plan_id: str, bucket_id: str, user_id: str):
        """
        Ref. to https://github.com/microsoftgraph/msgraph-sdk-python/issues/534
        :param plan_id:
        :param bucket_id:
        :param user_id:
        :return:
        """
        request_body = PlannerTask(
            plan_id=plan_id,
            bucket_id=bucket_id,
            title="Update client list",
            assignments=PlannerAssignments(
                additional_data={
                    user_id: {
                        "@odata.type": "#microsoft.graph.plannerAssignment",
                        "orderHint": " !",
                    },
                }
            ),
            start_date_time=datetime(2023, 12, 20, 10, 12, tzinfo=timezone.utc),
            due_date_time=datetime(2023, 12, 31, 12, 34, tzinfo=timezone.utc)
        )

        return await self.client.planner.tasks.post(request_body)
