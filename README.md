
# Microsoft Planner migration between tenants

Utility was written to simplify migration of Microsoft Planner plans inside or between
tenants.

## How to use

TBD

## Limitations

* Attachment migration is not implemented
* Python Graph SDK is used. This SDK is currently under development and some functionality
  may be broken later.
